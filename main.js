/*
Задание No1. Последнее значение цикла
Какое последнее значение выведет этот код? Почему? // 1
let i = 3;
while (i) {
alert( i-- );
}
 */
/*
Задание No2.
Даны три переменные вещественного типа: A, B, C. Если их значения упорядочены по
возрастанию, то удвоить их; в противном случае заменить значение каждой
переменной на противоположное. Вывести новые значения переменных A, B, C.
let A = prompt('Enter A');
let B = prompt('Enter B');
let C = prompt('Enter C');
if (A <= B && B <= C) {
    console.log(A * A);
    console.log(B * B);
    console.log(C * C);
} else {
    console.log(-A);
    console.log(-B);
    console.log(-C);
}
 */
/*
Задание No3.
Даны три переменные вещественного типа: A, B, C. Если их значения упорядочены по
возрастанию или убыванию, то удвоить их; в противном случае заменить значение
каждой переменной на противоположное. Вывести новые значения переменных A, B, C.
let A = prompt('Enter A');
let B = prompt('Enter B');
let C = prompt('Enter C');
if (A <= B && B <= C || A >= B && B >= C) {
    console.log(A * A);
    console.log(B * B);
    console.log(C * C);
} else {
    console.log(-A);
    console.log(-B);
    console.log(-C);
}
 */
/*
Задание No4.
На числовой оси расположены три точки: A, B, C. Определить, какая из двух последних
точек (B или C) расположена ближе к A, и вывести эту точку и ее расстояние от точки A.
Выделить код
let A = Math.abs(prompt('Enter A'));
let B = Math.abs(prompt('Enter B'));
let C = Math.abs(prompt('Enter C'));
let distanceFromAtoB = A - B;
let distanceFromAtoC = A - C;
if (distanceFromAtoB < distanceFromAtoC) {
    console.log('C' + ' is closer' + 'Distance from A to C is' + distanceFromAtoC);
} else if (distanceFromAtoC < distanceFromAtoB) {
    console.log('B' + ' is closer' + 'Distance from A to B is' + distanceFromAtoB);
} else if (distanceFromAtoC === distanceFromAtoB) {
    console.log('Points are equidistant');
}
 */
/*
Задание No5.
Даны целочисленные координаты точки на плоскости. Если точка совпадает с началом
координат, то вывести 0. Если точка не совпадает с начало координат, но лежит на оси
OX или OY, то вывести соответственно 1 или 2. Если точка не лежит на координатных
осях, то вывести 3.
let x = prompt('Enter X');
let y = prompt('Enter Y');
if (x == 0 && y == 0) {
    console.log(0);
} else if (x == 0) {
    console.log(1);
} else if (y == 0) {
    console.log(2);
}else {
    console.log(3);
}
 */
/*
Задание No6.
Даны координаты точки, не лежащей на координатных осях OX и OY. Определить
номер координатной четверти, в которой находится данная точка.

let x = prompt('Enter X');
let y = prompt('Enter Y');
if (x > 0 && y > 0) {
    console.log('First');
}
else if (x < 0 && y > 0) {
    console.log('Second');
}
else if (x < 0 && y < 0) {
    console.log('Third');
}else {
    console.log('Fourth')
}
 */
/*
Задание No7.
Даны целочисленные координаты трех вершин прямоугольника, стороны которого
параллельны координатным осям. Найти координаты его четвертой вершины.
let x1 = prompt('Enter X1');
let y1 = prompt('Enter Y2');
let x2 = prompt('Enter X3');
let y2 = prompt('Enter Y4');
let x3 = prompt('Enter X5');
let y3 = prompt('Enter Y6');
if (x2 == x3 ){
    console.log(x1);
}else if (x3 ==x1){
    console.log(x2);
}else {
    console.log(x3);
}
if (y2==y3){
    console.log(y1);
}else if (y3==y1){
    console.log(y2);
}else {
    console.log(y3);
}
 */
/*
Задание No8.
Дан номер года (положительное целое число). Определить количество дней в этом
году, учитывая, что обычный год насчитывает 365 дней, а високосный — 366 дней.
Високосным считается год, делящийся на 4, за исключением тех годов, которые
делятся на 100 и не делятся на 400 (например, годы 300, 1300 и 1900 не являются
високосными, а 1200 и 2000 — являются).
let numberOfYear = Math.abs(prompt('Enter year'));
if (numberOfYear % 4 == 0 && numberOfYear % 100 == 0 && numberOfYear % 400 == 0) {
    console.log('leap year (366 days)');
} else {
    console.log('year 365 days');
}
 */
/*
Задание No9.
Дано целое число. Вывести его строку-описание вида «отрицательное четное число»,
«нулевое число», «положительное нечетное число» и т. д.
let number = prompt('Enter number');
if (number == 0) {
    console.log('Zero');
} else {
    if (number % 2 == 0) {
        console.log('Even');
    } else{
        console.log('Odd number');
    }if (number < 0) {
        console.log('Negative');
    } else {
        console.log('Positive');
    }
}
 */
/*
Задание No10.
Дано целое число, лежащее в диапазоне 1–999. Вывести его строку- описание вида
«четное двузначное число», «нечетное трехзначное число» и т. д.
let number = prompt('Enter number');
if (number%2==0){
    console.log('Чётное');
}else {
    console.log('Нечетное');
}if (number>99){
    console.log('Трёхзначное');
}else if (99>=number && number>9){
    console.log('Двухзначное');
}else if (number<=9){
    console.log('Однозначное');
}
 */
/*
Задание No11. Какие значения выведет цикл while?
Для каждого цикла запишите, какие значения он выведет. Потом сравните с ответом.
Оба цикла выводят alert с одинаковыми значениями или нет? - Нет
Префиксный вариант ++i:
let i = 0;
while (++i < 5) alert( i );
Постфиксный вариант i++
let i = 0;
while (i++ < 5) alert( i );
 */
/*
Задание No12. Какие значения выведет цикл for?
Для каждого цикла запишите, какие значения он выведет. Потом сравните с ответом.
Оба цикла выведут alert с одинаковыми значениями или нет? - Да
Постфиксная форма:
for (let i = 0; i < 5; i++) alert( i );
Префиксная форма:
for (let i = 0; i < 5; ++i) alert( i );
 */
/*
Задание No13. Выведите чётные числа
При помощи цикла for выведите чётные числа от 2 до 10.
for (i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
        console.log(i);
    }
}
 */
/*
Задание No14. Замените for на while
Перепишите код, заменив цикл for на while, без изменения поведения цикла.
for (let i = 0; i < 3; i++) {
alert( `number ${i}!` );
}
let  i = 0;
while (i < 3) {
    alert(`number ${i}!`);
    i++;
}
 */
/*
Задание No15. Повторять цикл, пока ввод неверен
Напишите цикл, который предлагает prompt ввести число, большее 100. Если
посетитель ввёл другое число – попросить ввести ещё раз, и так далее.
Цикл должен спрашивать число пока либо посетитель не введёт число, большее 100,
либо не нажмёт кнопку Отмена (ESC).
Предполагается, что посетитель вводит только числа. Предусматривать обработку
нечисловых строк в этой задаче необязательно.
do {
    number = prompt('Enter number more than 100');
} while (number <= 100 && number);
 */
/*
Задание No16. Вывести простые числа
Натуральное число, большее 1, называется простым, если оно ни на что не делится,
кроме себя и 1.
Другими словами, n > 1 – простое, если при его делении на любое число от 1 до n есть
остаток.
Например, 5 это простое число, оно не может быть разделено без остатка на 2, 3 и 4.
Напишите код, который выводит все простые числа из интервала от 2 до n.
Для n = 10 результат должен быть 2,3,5,7.
P.S. Код также должен легко модифицироваться для любых других интервалов.

let n = 10;
for (let i = 2; i <= n; i++) {
    let number = true;
    for (let j = 2; j < i; j++) {
        if (i % j === 0) {
            number = false;
            break;
        }
    }
    if (number) {
        console.log(i);
    }
}
 */

/*
Задание No17.
Вычислить среднее арифметическое 2-ух чисел, введенных пользователем.
let result;
let number1 = prompt('Enter number1');
let number2 = prompt('Enter number2');
result = (+number1 + +number2) / 2;
console.log(result);

 */


/*
Задание No18.
Напишите скрипт, который вычислит квадрат любого введённого числа.
let number = prompt('Enter number');
number =  number * number;
console.log(number);
 */

/*
Задание No19.
Примите от пользователя 3 числа. Выведите на экран разницу большего и меньшего из
них.

let number1 = prompt('Enter number1');
let number2 = prompt('Enter number2');
let number3 = prompt('Enter number3');
let max;
let min;
let result;
if (number1 > number2 && number3) {
    max = number1;
} else if (number2 > number1 && number3) {
    max = number2;
} else if (number3 > number1 && number2) {
    max = number3;
}
if (number1 < number2 && number3) {
    min = number1;
} else if (number2 < number1 && number3) {
    min = number2;
} else if (number3 < number1 && number2) {
    min = number3;
}
result = max-min;
console.log(result);
 */
/*
Задание No20.
Напишите программу, проверяющую число, введенное с клавиатуры на четность.

let number = prompt('Enter number');
if (number % 2 == 0) {
    console.log('Event number');
}else {
    console.log('Not event number');
}
 */
/*
Задание No21.
Дано натуральное число а (a<100). Напишите программу, выводящую на экран
количество цифр в этом числе и сумму этих цифр
let number = prompt('Enter number(<100)');
let result;
if (number > 9) {
    console.log('Двухзначная');
    console.log(Math.trunc(result = +number / 10 + +number % 10));
} else {
    console.log('Однозначная'+number);
}

 */
/*
Задание No22.
Известно, что 1 дюйм равен 2.54 см. Разработать скрипт, переводящий дюймы в
сантиметры и наоборот. Диалог с пользователем реализовать через браузер

let inch = 2.54;
let number = prompt('Enter number of sm');
console.log('There are ' + +number / inch + '  inch in' + number + ' sm');

let number2 = prompt('Enter number of inch');
console.log('There are ' + +number2 * inch + ' inch in' + number2 + ' inch');
 */

/*
Задание No23. Найти сумму или произведение цифр трехзначного числа
Пользователь вводит через prompt трёхзначное число. Проверить трехзначное число
на четность и найти сумму его цифр, если число четное, или произведение его цифр,
если число нечетное.

let number = prompt('Enter number');
if (number % 2 == 0) {
    console.log(Math.abs(number % 10 + (Math.trunc(number / 10) % 10) + Math.trunc(number / 100)));
} else {
    console.log(Math.abs(number % 10 * (Math.trunc(number / 10) % 10) * Math.trunc(number / 100)));
}

 */
/*
Задание No24. Определить существование треугольника по трем сторонам
У треугольника сумма любых двух сторон должна быть больше третьей. Иначе две
стороны просто <лягут> на третью и треугольника не получится.
Пользователь вводит поочерёдно через prompt длины трех сторон.
Напишите код, который должен определять, может ли существовать треугольник при
таких длинах. Т. е. нужно сравнить суммы двух любых сторон с оставшейся третьей
стороной. Чтобы треугольник существовал, сумма всегда должна быть больше
отдельной стороны.

let number1 = prompt('Enter number1');
let number2 = prompt('Enter number2');
let number3 = prompt('Enter number3');
if (number1 + number2 > number3 && number1 + number3 > number2 && number2 + number3 > number1) {
    console.log('The triangle exists');
} else {
    console.log('The triangle does not exist');
}

 */
/*
Задание No25. Принадлежность точки окружности
Даны координаты точки A(x = 4, y = 9) и радиус окружности (R = 10) с центром в начале
координат.
Напишите код, который будет выводить сообщение о том, лежит ли данная точка
внутри окружности или за её пределами. Для извлечения квадратного корня из числа z
вам понадобится метод Math.sqrt(z).
Например: let a = Math.sqrt(4); // a=2

let a = [7, 5];
let circleRadius = 10;
if (a[0]**2 + a[1]**2 <= circleRadius**2) {
    console.log('Inside the circle');
} else {
    console.log('Not inside a circle');
}

 */

/*
Задание No26. Объекты
Напишите код, выполнив задание из каждого пункта отдельной строкой:
• Создайте пустой объект user.
• Добавьте свойство name со значением John.
• Добавьте свойство surname со значением Smith.
• Измените значение свойства name на Pete.
• Удалите свойство name из объекта.
let user = {};
user.name = 'John';
user.surname = 'Smith';
user.name = 'Pete';
delete user.name;

 */

/*
Задание No27. Объекты-константы
Можно ли изменить объект, объявленный с помощью const?
const user = {
name: "John"
};
user.name = "Pete"; // это будет работать? - Да,будет,так как нельзя изменить ссылку на объект,но само содержание объекта менять можно
 */

/*
Задание No28. Сумма свойств объекта
Есть объект, в котором хранятся зарплаты нашей команды:
let salaries = {
John: 100,
Ann: 160,
Pete: 130
}
Напишите код для суммирования всех зарплат и сохраните результат в переменной
sum. Должно получиться 390.
Если объект salaries пуст, то результат должен быть 0.
let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
};
let result = 0;
for (let key in salaries) {
    result += salaries[key];
}
console.log(result);

 */
/*
Задание No29. Бесконечный цикл по ошибке
Этот цикл – бесконечный. Он никогда не завершится, почему?- Потому что значение не будет равно 10
let i = 0;
while (i != 10) {
i += 0.2;
}
 */
